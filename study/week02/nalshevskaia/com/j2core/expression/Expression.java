package study.week02.nalshevskaia.com.j2core.expression;

import java.lang.Math;
import java.util.Scanner;

public class Expression {
    private static final String WRONG = "Expression is totally wrong.";
    private static final String LINEAR = "This is linear expression. There is one result: ";
    private static final String D_LESS_ZERO =
            "Discriminant is less than zero. This expression has no rational solutions.";
    private static final String D_IS_ZERO = "Discriminant is zero. There is one result: ";
    private static final String RESULTS = "Results of square expression are: ";
    private static final String ASK_PARAMETERS = "Please, enter all expression parameters using space or enter: ";
    private static final String AND = " and ";
    private static final String NAN = "No this is not numbers! Try again ;)";
    private static final String THREE_PARAMETER_ANNOUNCEMENT = "We'll use just your first three NUMBERS))";
    private static final double ERROR = 0.000001D;

    private static double x1, x2;

    private static double discriminant(double a, double b, double c) {
        return Math.pow(b, 2) - 4 * a * c;
    }

    private static void solveEquation(double[] parameters) {
        if (parameters[0] == 0) {
            if (parameters[1] == 0) {
                System.out.print(WRONG);
            } else {
                x1 = -parameters[2] / parameters[1];
                System.out.println(LINEAR + x1);
            }
        } else {
            double discriminant = discriminant(parameters[0], parameters[1], parameters[2]);
            if (discriminant < -ERROR) {
                System.out.println(D_LESS_ZERO);
            } else {
                if (discriminant > ERROR) {
                    double sqrtValue = Math.sqrt(discriminant);
                    x1 = (-parameters[1] + sqrtValue) / 2 / parameters[0];
                    x2 = (-parameters[1] - sqrtValue) / 2 / parameters[0];
                    System.out.println(RESULTS + x1 + AND + x2);
                } else {
                    x1 = -parameters[1] / 2 / parameters[0];
                    System.out.println(D_IS_ZERO + x1);
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(ASK_PARAMETERS);
        double[] parameters = new double[3];
        int inputCounter = 0;
        while (scanner.hasNext()) {
            String nextInput = scanner.next();
            try {
                parameters[inputCounter++] = Double.parseDouble(nextInput);
                if (inputCounter >= parameters.length){
                    System.out.println(THREE_PARAMETER_ANNOUNCEMENT);
                    break;
                }
            } catch (NumberFormatException e){
                inputCounter--;
                System.out.println(NAN);
            }
        }
        solveEquation(parameters);
    }
}
